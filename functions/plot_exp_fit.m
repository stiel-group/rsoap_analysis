function plot_exp_fit (exp_fit_normal, exp_fit_reversed, exp_fit_both,mean_cycle_normalized, plot_title)

current_figure = 1;

pulses_num = size (mean_cycle_normalized,3);

figure('Position',get(0,'ScreenSize'));%figure(current_figure);
sgtitle (plot_title);

subplot (3, 3, 1);
imagesc(exp_fit_normal.b, [-0.5, 0]); colorbar;
title ('exp fit, normal');

subplot (3, 3, 2);
imagesc(exp_fit_reversed.b, [-0.5, 0]); colorbar;
title ('exp fit, reversed');

subplot (3, 3, 3);
imagesc(exp_fit_both.b, [-0.5, 0]); colorbar;
title ('exp fit, both');

subplot (3, 3, 4);
imagesc(exp_fit_normal.rsqr); colorbar;
title ('exp fit, R-squared, normal');

subplot (3, 3, 5);
imagesc(exp_fit_reversed.rsqr); colorbar;
title ('exp fit, R-squared, reversed');

subplot (3, 3, 6);
imagesc(exp_fit_both.rsqr); colorbar;
title ('exp fit, R-squared, both');

button_main = 1;

x_points = 1:pulses_num;

while (button_main == 1)  %when right mouse button clicked - closed        
    figure (current_figure);
    [x, y, button_main] = ginput(1);
    if (button_main ~= 1) 
        close(current_figure); %when right mouse button clicked - closed
        break;
    end
        
    x = round(x); 
    y = round(y);
   
    % mark chosen point on other subplots

subplot (3, 3, 1);
imagesc(exp_fit_normal.b, [-0.5, 0]); colorbar;
title ('exp fit, normal');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

subplot (3, 3, 2);
imagesc(exp_fit_reversed.b, [-0.5, 0]); colorbar;
title ('exp fit, reversed');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


subplot (3, 3, 3);
imagesc(exp_fit_both.b, [-0.5, 0]); colorbar;
title ('exp fit, both');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


subplot (3, 3, 4);
imagesc(exp_fit_normal.rsqr); colorbar;
title ('exp fit, R-squared, normal');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


subplot (3, 3, 5);
imagesc(exp_fit_reversed.rsqr); colorbar;
title ('exp fit, R-squared, reversed');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;


subplot (3, 3, 6);
imagesc(exp_fit_both.rsqr); colorbar;
title ('exp fit, R-squared, both');
    hold on;
    plot( x, y, 'r+', 'MarkerSize', 5, 'LineWidth', 1);        
    hold off;

    % plot trajectories
    
   
    subplot(3, 1, 3);
        
    y_norm = squeeze(mean_cycle_normalized (y,x, :));
    if (exp_fit_normal.b(y,x) ~= 0)
        exp_b = exp_fit_normal.b(y,x);     
        exp1 = @(x_val)exp(exp_b*(x_val-1));
        Yfitted = exp1(x_points');
        
        plot (x_points', y_norm, 'r-',x_points', Yfitted, 'b--');
        title ('mean cycle nomalized');        
        legend ( 'normalized', 'expected' );
    else
    if (exp_fit_reversed.b(y,x) ~= 0)
        exp_b = exp_fit_reversed.b(y,x);
        exp1 = @(x_val)(-exp(exp_b*(x_val-1))+1);
        Yfitted = exp1(x_points');
        plot (x_points', y_norm, 'r-',x_points', Yfitted, 'b--');
        title ('mean cycle nomalized');        
        legend ( 'normalized', 'expected' );
    else
        plot(y_norm);
    end
    end
        
    
    
end
