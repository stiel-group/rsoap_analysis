function [Factors_values, Class_lables] = get_factors_values (roi, exp_fit_both, exp_fit_normal, exp_fit_reversed, ffts, basic_analysis_values, cycles_analysis_values)

p_roi = roi.p_roi;
mouse_region = roi.mouse_region;

%only points for which exp fit was estimated 
p_roi_estimated = p_roi.* (exp_fit_both.rsqr~=0);
mouse_region_estimated = mouse_region.* (exp_fit_both.rsqr~=0);


number_points = sum(mouse_region_estimated(:)); %only points for which exp fit was estimated 
x_coord = zeros(number_points, 1);
y_coord = zeros(number_points, 1);

% factors:
% rsqr 
% exp.b
% mean_intensities
% amplitudes
% neative_vals_count
% max_pos_median
% min_pos_median
% max_pos_std
% min_pos_std
% good_cycles_num
% cycle_length

number_factors = 13;
Factors_values = zeros(number_points, number_factors);
Class_lables = char(number_points,1);

x_size = size(Anatomy_img, 1);
y_size = size(Anatomy_img, 2);

p_ind = 0;

for x_ind = 1:x_size
    for y_ind = 1:y_size
        if (p_roi_estimated(x_ind,y_ind))
            p_ind = p_ind + 1;
            x_coord(p_ind) =  x_ind;
            y_coord(p_ind) =  y_ind;
            Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
            Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
            Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
            Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
            Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
            Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
            Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
            Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
            Factors_values (p_ind, 13) = ffts(x_ind,y_ind);

            Class_lables (p_ind) = 's'; 
        else
            if (mouse_region_estimated(x_ind,y_ind))
            p_ind = p_ind + 1;
            x_coord(p_ind) =  x_ind;
            y_coord(p_ind) =  y_ind;
            Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
            Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
            Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
            Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
            Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
            Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
            Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
            Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
            Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
            Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
            Factors_values (p_ind, 13) = ffts(x_ind,y_ind);

            Class_lables (p_ind) = 'b'; 
            end
        end 
    end
end

end