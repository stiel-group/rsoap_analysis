function [basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (R)

x_size = size (R,1);
y_size = size (R,2);
cycles_num = size (R,3);
pulses_num = size (R,4);

Anatomy_img = R (:,:,1,1); % first frame, first pulse
%tic
%Anatomy_img = get_anatomy_img (R (:,:,1,1:pulses_num));
%toc
%disp('anatomy img');
% mean trajectory cycle
tic

trajectories.mean_cycle = squeeze(mean (R,3));
trajectories.mean_cycle_normalized = normalize(trajectories.mean_cycle,3,'range');

toc;
disp ('trajectories');

tic 

%mean_trajectory_cycle_normalized  = normalize (mean_trajectory_cycle);

intensities_concatenated = reshape(permute(R,[1,2,4,3]), x_size, y_size, pulses_num*cycles_num) ;

basic_analysis_values.amplitudes = abs (max(intensities_concatenated,[],3) - min(intensities_concatenated,[],3));

basic_analysis_values.mean_intensities = mean(intensities_concatenated,3);

basic_analysis_values.negative_vals_count = sum(R<0, [3 4]);


%expected curve slope
exp_b = -0.3;

tic
[residuals.normal, residuals.reversed, residuals.both]= get_residuals (trajectories.mean_cycle_normalized,exp_b);
toc;
disp ('residuals');


tic
Y = fft(intensities_concatenated,[],3);
ffts = squeeze(abs (Y(:,:,cycles_num + 1)));
        
toc;
disp ('fft');


end
