function registered_img = get_registered_img (refference_img, proc_img, use_non_rigid)

if use_non_rigid
    
    %[500 400 200] number of iterations per pyramid level (resolution level)
    % AccumulatedFieldSmoothing - Smoothing applied at each iteration (also can be specified for each pyramid level)
    [~,img_nonrigid_reg] = imregdemons(proc_img,refference_img,[500 400 200],'AccumulatedFieldSmoothing',1, 'DisplayWaitbar', false); 
    
  %  [optimizer, metric] = imregconfig('monomodal');

  %  registered_img = imregister(img_nonrigid_reg, refference_img, 'affine', optimizer, metric);
   registered_img = img_nonrigid_reg;
else
    
    [optimizer, metric] = imregconfig('monomodal');

    affine_reg = imregister(proc_img,refference_img, 'affine', optimizer, metric);
  %  affine_reg = proc_img;
    tformEstimate = imregcorr(affine_reg,refference_img);
    Rfixed = imref2d(size(refference_img));
    registered_img = imwarp(affine_reg,tformEstimate,'OutputView',Rfixed);
    
    
end

end