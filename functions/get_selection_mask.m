function mask = get_selection_mask (rsqr_limit,good_cycles_num_lim,include_reversed,exp_fit_normal,exp_fit_both,cycles_analysis_values)

%mask.indexes =  (cycles_analysis_values.good_cycles_num >= good_cycles_num_lim);

selected_based_on_cycles = cycles_analysis_values.max_pos_matching_c_num + cycles_analysis_values.min_pos_matching_c_num;
mask.indexes = selected_based_on_cycles>=good_cycles_num_lim;

if (include_reversed)
   mask.indexes = mask.indexes.*( mask.indexes == (exp_fit_both.rsqr > rsqr_limit)); 
else
   mask.indexes = mask.indexes.*( mask.indexes == (exp_fit_normal.rsqr > rsqr_limit));
end

mask.params.rsqr_limit = rsqr_limit;
mask.params.good_cycles_num_lim = good_cycles_num_lim;
mask.params.include_reversed = include_reversed;

end