function cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta)

x_size = size(intentsity_cycles,1);
y_size = size(intentsity_cycles,2);

[cycles_analysis_values.max_val, cycles_analysis_values.max_pos] = max (intentsity_cycles, [], 3);
cycles_analysis_values.max_pos_median = median (squeeze(cycles_analysis_values.max_pos),3);
cycles_analysis_values.max_pos_std = std (squeeze(cycles_analysis_values.max_pos),[],3);
[cycles_analysis_values.min_val, cycles_analysis_values.min_pos] = min (intentsity_cycles, [], 3);
cycles_analysis_values.min_pos_median = median (squeeze(cycles_analysis_values.min_pos),3);
cycles_analysis_values.min_pos_std = std (squeeze(cycles_analysis_values.min_pos),[],3);

cycles_analysis_values.max_pos_matching_c_num = zeros(x_size, y_size);
cycles_analysis_values.min_pos_matching_c_num = zeros(x_size, y_size);
cycles_num = size(cycles_analysis_values.min_pos, 4);
pulses_num = size(intentsity_cycles,3);
cycles_analysis_values.max_pos_matching_c_ind = zeros (x_size, y_size, cycles_num);
cycles_analysis_values.min_pos_matching_c_ind = zeros (x_size, y_size, cycles_num);

for x_ind = 1:x_size
    for y_ind = 1:y_size
        if (cycles_analysis_values.min_pos_median(x_ind, y_ind) <=delta.begining)
            cycles_analysis_values.min_pos_matching_c_ind (x_ind, y_ind, :)= abs (cycles_analysis_values.min_pos(x_ind, y_ind, 1, :) - cycles_analysis_values.min_pos_median(x_ind, y_ind)) < delta.begining;
            cycles_analysis_values.min_pos_matching_c_num (x_ind, y_ind) = sum  (cycles_analysis_values.min_pos_matching_c_ind(x_ind, y_ind, :));
        end
        if (cycles_analysis_values.max_pos_median(x_ind, y_ind) <=delta.begining)
            cycles_analysis_values.max_pos_matching_c_ind  (x_ind, y_ind, :)= abs (cycles_analysis_values.max_pos(x_ind, y_ind, 1, :) - cycles_analysis_values.max_pos_median(x_ind, y_ind)) < delta.begining;
            cycles_analysis_values.max_pos_matching_c_num (x_ind, y_ind) = sum  (cycles_analysis_values.max_pos_matching_c_ind(x_ind, y_ind, :));
        end
        
        if (cycles_analysis_values.min_pos_median(x_ind, y_ind) >= (pulses_num - delta.end))
            cycles_analysis_values.min_pos_matching_c_ind (x_ind, y_ind, :)= abs (cycles_analysis_values.min_pos(x_ind, y_ind, 1, :) - cycles_analysis_values.min_pos_median(x_ind, y_ind)) < delta.end;
            cycles_analysis_values.min_pos_matching_c_num (x_ind, y_ind) = sum  (cycles_analysis_values.min_pos_matching_c_ind(x_ind, y_ind, :));
        end
        if (cycles_analysis_values.max_pos_median(x_ind, y_ind) >= (pulses_num - delta.end))
            cycles_analysis_values.max_pos_matching_c_ind  (x_ind, y_ind, :)= abs (cycles_analysis_values.max_pos(x_ind, y_ind, 1, :) - cycles_analysis_values.max_pos_median(x_ind, y_ind)) < delta.end;
            cycles_analysis_values.max_pos_matching_c_num (x_ind, y_ind) = sum  (cycles_analysis_values.max_pos_matching_c_ind(x_ind, y_ind, :));
        end
    end
end

cycles_analysis_values.good_cycles = (cycles_analysis_values.min_pos_matching_c_ind == cycles_analysis_values.max_pos_matching_c_num);

cycles_analysis_values.good_cycles_num = sum (cycles_analysis_values.good_cycles, 3);

cycles_analysis_values.cycle_length = abs(cycles_analysis_values.max_pos_median - cycles_analysis_values.min_pos_median);

end