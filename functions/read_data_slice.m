function Reconstruction_slice = read_data_slice (recon_path, slice_idx, wl_idx)

selMat = read(recon_path);
selMat = selMat(:,slice_idx,wl_idx,:);

end
