*multilabeling models can be found in a corresponding model folder*

1. For peforming multilabeling analysis
1.1 Load the modelto use for analysis
load ('Mdl_name.mat','Mdl');
1.2 Adjust analysed scan info
loadfolder = 'C:\full_basic_analysis\'; 
sample_name = 'Jurkats_old_Conc_2';
wl = '770';

2. For adding a new scan to an existing model 
2.1 run build_ml_model.m part on added scan
2.2 run adding_to_existing_model_script
2.3 run %% build decision tree based on factor values of build_ml_model.m


