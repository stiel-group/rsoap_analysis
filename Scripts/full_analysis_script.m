%% add functions path - edit if needed !!!

addpath ('..\analysis_functions');

%% read scan
% !!! change several_days and day_ind if needed !!!

recon_path = 'data.mat';
slice_idx = 1;
wl_idx = 2;

[Reconstruction_slice] = read_data_slice (recon_path, slice_idx, wl_idx);

sample_name = Reconstruction_slice.datainfo.Name;
recon_name = Reconstruction_slice.datainfo.ReconNode.Name;
wl = int2str (Reconstruction_slice.wl_proc_value);
slice = int2str (slice_idx);

%% basic analysis

[basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);

%% movement correction

intentsity_cycles = permute(reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3)),[1 2 3 4]);
Anatomy_img = get_anatomy_img (intentsity_cycles (:,:,1,1:11));
[basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (intentsity_cycles);

use_non_rigid = 0;
Recon_movement_corrected = get_movement_corrected_recon (intentsity_cycles, Anatomy_img, use_non_rigid);

%% save basic analysis values

savefolder = 'C:\full_basic_analysis\';

slice = int2str(slice_idx);

if ~several_days % 1 if several days for sample !!!
    savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    save (savename, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','several_days','-v7.3');
else
    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    save (savename, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','several_days','day_ind','-v7.3');
end

%% load basic analysis (if already processed)
% !!! change several_days and day_ind if needed !!!

%loadfolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

loadfolder = 'C:\full_basic_analysis\';

%sample_name = strcat(sample_name,'_non_rigid_movement_corrected');

sample_name = 'ReAgar_2%BloodAgarCore';
%sample_name = 'gastro_mouse';

%sample_name = strcat(sample_name,'_non_rigid_movement_corrected');

wl = '770';
slice = '2';

several_days = 0; % 1 if several days for sample !!!
day_ind = 14;      % day ind, for several days for sample !!!

if ~several_days % 1 if several days for sample !!!
    loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    load (loadname, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','several_days');
else
    loadname = strcat (loadfolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    load (loadname, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','several_days','day_ind');
end

%% display basic analysis values

plot_title = strcat('ScanName =', strrep(sample_name,'_',' '), '; wl = ',wl,', slice = ', int2str(slice_idx) );

if several_days % 1 if several days for sample !!!
    plot_title = strcat(plot_title, ', day ', int2str(day_ind));
end

plot_basic_analysis_values (basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img, plot_title);

%% choose limits for residuals
figure(1);
subplot(2,2,1)
imshow(Anatomy_img, [0 300])
subplot(2,2,3)
limit_for_normal = 1;
limit_for_reversed = 1;
imagesc ((residuals.normal<limit_for_normal).*residuals.normal); colorbar;
title('only normal');
subplot(2,2,4)
imagesc ((residuals.both<limit_for_normal).*residuals.both); colorbar;
title('normal and reverse');
sgtitle(strcat('slice ', int2str(slice_idx)));

%% exp fit (for chosen residuals' limits)

limit_for_normal = 1;
limit_for_reversed = 1;

Mask_normal = residuals.normal < limit_for_normal;
Mask_reversed = residuals.reversed < limit_for_reversed;
pulses_num = size(trajectories.mean_cycle_normalized,3);
pulses_num = 11;

[exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);

%% save exp fit

%savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';
savefolder = 'C:\full_basic_analysis\';

if ~several_days % 1 if several days for sample !!!
    savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
    save (savename, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days','-v7.3');
else
    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
    save (savename, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days','day_ind','-v7.3');
end

%% load exp fit (if already processed)
% !!! change several_days and day_ind if needed !!!

%loadfolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

loadfolder = 'C:\full_basic_analysis\';

sample_name = 'ReAgar_CoreRe';
wl = '770';
slice = '2';

several_days = 0; % 1 if several days for sample !!!
day_ind = 1;      % day ind, for several days for sample !!!

if ~several_days % 1 if several days for sample !!!
    loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
    load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days');
else
    loadname = strcat (loadfolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
    load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days','day_ind');
end

%% display exp fit

plot_title = strcat('ScanName =', strrep(sample_name,'_',' '), '; wl = ',wl,', slice = ', int2str(slice_idx) );

if several_days
    plot_title = strcat(plot_title, ', day ', int2str(day_ind));
end

%plot_exp_fit (exp_fit_normal, exp_fit_reversed, exp_fit_both,trajectories.mean_cycle_normalized,plot_title);

plot_exp_fit_1 (exp_fit_normal, exp_fit_reversed, exp_fit_both,trajectories.mean_cycle_normalized,plot_title, Anatomy_img);

%% display only selection
rsqr_limit = 0;
subplot(1,2,1);
imagesc(Anatomy_img); %colormap('gray');freezeColors
colormap(gca,'gray')

subplot(1,2,2);
imagesc((exp_fit_both.rsqr>rsqr_limit).*exp_fit_both.b , [-0.5 -0.2]);colorbar;
colormap(gca,'default');

%% get fluency correction 
[roi_min_distances, roi_fluency] = get_fluency_correction (Anatomy_img);

%% save fluency correction

%savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

savefolder = 'C:\full_basic_analysis\';

if ~several_days % 1 if several days for sample !!!
    savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_fluency_correction.mat');
else
    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_fluency_correction.mat');
end

save (savename, 'roi_min_distances','roi_fluency','-v7.3');

%% save Anatomy as .png

I = Anatomy_img - min(Anatomy_img(:));
I = I ./ max(I(:));
Anatomy_img_scaled = imresize(I,[900 900]);
imshow(Anatomy_img_scaled);

imwrite(Anatomy_img_scaled, strcat(sample_name,'_anatomy.png'));

%% save unmixing as png
%kinetics_limit = -0.5;

max_rsqr = 0.9848;

%TransparencyData = ((exp_fit_normal.rsqr>rsqr_limit).* exp_fit_normal.rsqr.*(roi_fluency~=0));
%TransparencyData = exp_fit_both.rsqr .* mask.indexes.*(roi_fluency~=0);
%TransparencyData =(exp_fit_normal.rsqr>rsqr_limit).* exp_fit_both.rsqr .*(roi_fluency~=0);

%TransparencyData = mask.indexes.*exp_fit_normal.b*-1; % to make values positive for converting to image
TransparencyData = mask.indexes.*exp_fit_normal.rsqr; 

%rsqr_total(1,1) = kinetics_limit;%max_rsqr; 
rsqr_total(1,1) = max_rsqr; 


%TransparencyData = rsqr_total.*(rsqr_total>rsqr_limit).*(roi_fluency~=0);

TransparencyData (1,1) = 0;

unmix_img = ind2rgb(im2uint8((TransparencyData)), parula(256));

backgroundcolor = ind2rgb(im2uint8(mat2gray(0)), parula(256));

delta_color = 0.1;%0.6; - for rsqr

unmix_img_scaled = imresize(unmix_img,[900 900]);

for x_ind=1:900
    for y_ind = 1:900
        if sum(squeeze(unmix_img_scaled(x_ind,y_ind,:) - backgroundcolor))<delta_color
            unmix_img_scaled(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end

imshow(unmix_img_scaled);

imwrite(unmix_img_scaled,strcat(sample_name,'_rsqr.png'),'Transparency', ind2rgb(im2uint8(mat2gray(0)), parula(256)) );
img = imread(strcat(sample_name,'_unmixing.png'));
unmix_img_scaled = imresize(img,[900 900]);
imwrite(umix_img_scaled,strcat(sample_name,'_unmixing.png'),'Transparency',[0 0 0] );


%% save overlay as png

%img_unmix = imread(strcat(sample_name,'_clusters.png'));
%img_unmix = imread(strcat(sample_name,'_kinetics.png'));
img_unmix = imread(strcat(sample_name,'_rsqr.png'));

%img_unmix = imread(strcat(sample_name,'_day_1_unmixing.png'));
img_anatomy = imread(strcat(sample_name,'_anatomy.png'));

img_anatomy_cutted = cat(3, img_anatomy, img_anatomy, img_anatomy);

img_anatomy_cutted (img_unmix~=0) = 0;

img_overlay = img_unmix + img_anatomy_cutted;
imshow(img_overlay);
imwrite(img_overlay,strcat(sample_name,'_overlayed_rsqr.png') );
%% save hist 

%img_color = imread(strcat('Z:\Data\CellEngineering\1804_KM_Switching\HCT_MIce_2_GFP\',int2str(i),'1GFP_20secG20.jpg'));
img_scaled = imresize(img_color,[900 900]);
imshow(img_scaled);
imwrite(img_scaled, strcat(sample_name,'_hist.jpg'));
%%
img_scaled = imresize(img_gfp,[900 900]);
imshow(img_scaled);
imwrite(img_scaled, strcat(sample_name,'_gfp.jpg'));
%%
for i=15:75
img_gfp = imread(strcat('Z:\Data\CellEngineering\1804_KM_Switching\HCT_MIce_2_GFP\',int2str(i),'GFP_10secG10.jpg'));
img_gfp = flipud(img_gfp);
img_scaled = imresize(img_gfp,[900 900]);
imshow(img_scaled);
imwrite(img_scaled, strcat('C:\2_histology_img\hct_',int2str(i),'_gfp.tiff'));
end
%% mark selection image

RGB = zeros(size(Anatomy_img));

[x_pos y_pos]   = find(mask.indexes == 1);
pos = [y_pos x_pos];
RGB = insertMarker(RGB,pos,'o','color','blue','size',1);


%% mark clusters image
RGB = zeros(size(Anatomy_img));


[x_pos y_pos]   = find(data_clustered.indexes(:,:,2) == 1);
pos = [y_pos x_pos];
RGB = insertMarker(RGB,pos,'o','color','green','size',1);

[x_pos y_pos]   = find(data_clustered.indexes(:,:,1) == 1);
pos = [y_pos x_pos];
RGB = insertMarker(RGB,pos,'o','color','blue','size',1);

[x_pos y_pos]   = find(data_clustered.indexes(:,:,3) == 1);
pos = [y_pos x_pos];
RGB = insertMarker(RGB,pos,'o','color','red','size',1);


imshow(RGB);

%% save cluster image as .png 
cluster_img = imresize(RGB,[900, 900]);

backgroundcolor = [0 0 0];

delta_color = 1.2;

for x_ind=1:900
    for y_ind = 1:900
        if sum(abs(squeeze(cluster_img(x_ind,y_ind,:) - backgroundcolor)))<delta_color
            cluster_img(x_ind,y_ind,:) = backgroundcolor;
        end
    end
end


imshow(cluster_img);

imwrite(cluster_img,strcat(sample_name,'_clusters.png'),'Transparency', [0 0 0] );


%% clustering based on expected values

clusters.b_values = [-0.21 -0.11 -0.06];
clusters.delta = [0.06  0.009 0.015];

rsqr_limit = 0.6;

mouse_roi = (roi_fluency>0);

exp_fit_normal.b = exp_fit_normal.b.*mouse_roi;

data_clustered = get_clusters_by_values (exp_fit_normal, trajectories.mean_cycle_normalized, rsqr_limit, clusters);

plot_clusters(data_clustered);

%% clustering by kmeans
clusters_num = 2;
rsqr_limit = 0.9;
data_clustered = get_clusters_by_kmeans (exp_fit_normal, trajectories.mean_cycle_normalized, rsqr_limit, clusters_num);
plot_clusters(data_clustered);


%% cycles analysis

delta.begining = 1; % for cycle first index (from end for reversed ones) 
delta.end = 1;      % for cycle last index  (from end for reversed ones)

intentsity_cycles = reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3));

cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta);
%% display cycles analysis

plot_cycles_analysis_values (cycles_analysis_values, intensities_concatenated)

%% save cycles analysis values

%savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

savefolder = 'C:\full_basic_analysis\';

if ~several_days % 1 if several days for sample !!!
    savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
else
    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
end

save (savename, 'cycles_analysis_values','sample_name','recon_name','wl','slice_idx','-v7.3');


%% selection based on cycles analysis
selected_based_on_cycles = cycles_analysis_values.max_pos_matching_c_num + cycles_analysis_values.min_pos_matching_c_num;
mask_limit = 20;
selected_based_on_cycles_mask = selected_based_on_cycles>mask_limit;
imagesc(selected_based_on_cycles); colorbar;

%% get selections

rsqr_limit = 0.7;        % R-square for exp fitting quality
include_reversed = 0;    % 1 if included 
good_cycles_num_lim = 0;%20; % 0 if not limited

%selected_based_on_cycles_mask = selected_based_on_cycles>mask_limit;


mask = get_selection_mask (rsqr_limit,good_cycles_num_lim,include_reversed,exp_fit_normal,exp_fit_both,cycles_analysis_values);

%mask.indexes = selected_based_on_cycles_mask;
%mask.params.rsqr_limit = 0.6;
%mask.params.good_cycles_num_lim = 20;
%mask.params.include_reversed = 1;


selection = get_selections (mask,exp_fit_both.rsqr, ffts, basic_analysis_values.mean_intensities,roi_fluency);

plot_selections(selection);

%%
for i=5:9
img = imread(strcat(sample_name,'_overlayed_slice_',int2str(i),'.png') );
    subplot(2,3,i-4);
   imshow(img);
end
