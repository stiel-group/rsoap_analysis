function calculation (data_path, sample_name, slice_idx, wl_idx)

addpath ('..\analysis_functions');

[Reconstruction_slice] = read_data_slice (data_path, slice_idx, wl_idx);

sample_name = Reconstruction_slice.datainfo.Name;
recon_name = Reconstruction_slice.datainfo.ReconNode.Name;
wl = int2str (Reconstruction_slice.wl_proc_value);
slice = int2str (slice_idx);

%% movement correction

intentsity_cycles = permute(reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3)),[1 2 3 4]);
optimal_pulses = 11;
Anatomy_img = get_anatomy_img (intentsity_cycles (:,:,1,1:optimal_pulses));
use_non_rigid = 0;
Recon_movement_corrected = get_movement_corrected_recon (intentsity_cycles, Anatomy_img, use_non_rigid);

%% basic analysis

[basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);


%% save basic analysis values

savefolder = '..\';

slice = int2str(slice_idx);

savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
save (savename, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','several_days','day_ind','-v7.3');

%% change the limits if needed to speed up the process

limit_for_normal = 1;
limit_for_reversed = 1;

Mask_normal = residuals.normal < limit_for_normal;
Mask_reversed = residuals.reversed < limit_for_reversed;
pulses_num = size(trajectories.mean_cycle_normalized,3);
pulses_num = 11;

[exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);

%% save exp fit

savefolder = '..\';

    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
    save (savename, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days','day_ind','-v7.3');


%% get fluency correction 
[roi_min_distances, roi_fluency] = get_fluency_correction (Anatomy_img);

%% save fluency correction

%savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

savefolder = 'C:\full_basic_analysis\';

if ~several_days % 1 if several days for sample !!!
    savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_fluency_correction.mat');
else
    savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_fluency_correction.mat');
end

save (savename, 'roi_min_distances','roi_fluency','-v7.3');


%% cycles analysis

delta.begining = 1; % for cycle first index (from end for reversed ones) 
delta.end = 1;      % for cycle last index  (from end for reversed ones)

intentsity_cycles = reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3));

cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta);
%% display cycles analysis

plot_cycles_analysis_values (cycles_analysis_values, intensities_concatenated)

%% save cycles analysis values

savefolder = '..\';

savename = strcat (savefolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
save (savename, 'cycles_analysis_values','sample_name','recon_name','wl','slice_idx','-v7.3');