training (model_name, sample_name, slice_idx, wl_idx, already_existing)

if already_existing 
    load (strcat(model_name,'.mat'), Mdl);
    Values = Mdl.X;
    FactorTable =[FactorTable;Values];
    Labels = Mdl.Y;
    Labels = char(Labels);
    Class_lables_all =[Class_lables_all;Labels];
else
    Class_lables_all = [];
    Values = [];
end

rsqr = [];
exp_b_normal = [];
exp_b_reversed = [];
mean_intensities = [];
amplitudes = [];
negative_vals_count = [];
max_pos_median = [];
min_pos_median = [];
max_pos_std = [];
min_pos_std = [];
good_cycles_num = [];
cycle_length = [];
ffts_right_freq = [];

%% load data - repeat for every scan used for model

loadfolder = '..\'; 

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','recon_name','wl','slice_idx');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');

%first select the whole mouse, after - roi
[roi.p_roi, roi.mouse_region] = get_roi (Anatomy_img);

[Factors_values, Class_lables] = get_factors_values (roi, exp_fit_both, exp_fit_normal, exp_fit_reversed, ffts, basic_analysis_values, cycles_analysis_values);

rsqr = [rsqr; Factors_values(:, 1) ];
exp_b_normal = [exp_b_normal; Factors_values(:, 2)];
exp_b_reversed = [exp_b_reversed; Factors_values(:, 3)];
mean_intensities = [mean_intensities; Factors_values(:, 4)];
amplitudes = [amplitudes; Factors_values(:, 5)];
negative_vals_count = [negative_vals_count; Factors_values(:, 6)];
max_pos_median = [max_pos_median; Factors_values(:, 7)];
min_pos_median = [min_pos_median; Factors_values(:, 8)];
max_pos_std = [max_pos_std; Factors_values(:, 9)];
min_pos_std = [min_pos_std; Factors_values(:, 10)];
good_cycles_num = [good_cycles_num; Factors_values(:, 11)];
cycle_length = [cycle_length; Factors_values(:, 12)];
ffts_right_freq = [ffts_right_freq; Factors_values(:, 13)];

Class_lables_all = [Class_lables_all; Class_lables];
FactorTable = table(rsqr,exp_b_normal,exp_b_reversed,ffts_right_freq,mean_intensities,amplitudes,negative_vals_count,max_pos_median,min_pos_median,max_pos_std,min_pos_std,good_cycles_num,cycle_length);

%% build decision tree based on factor values
%%
rng(1); % For reproducibility
Mdl = TreeBagger(300,FactorTable,Class_lables_all,'OOBPrediction','On',...
    'OOBPredictorImportance', 'On', 'Method','classification');

save (Mdl, 'model_name')
