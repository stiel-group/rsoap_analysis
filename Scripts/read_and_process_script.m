%% process scan slice and save results

%% add path in 'analysis_functions' folder

addpath ('N:\IBMI Transfer\MSOT_GUI_CellEngineering_Version_NEW\_switching_recon_and_analysis_ms\full_analysis\analysis_functions');

%% read slice from recon

recon_path = 'Z:\Data\CellEngineering\1804_KM_Switching\Study_397_Tcells_iRFP_Swit_qm\Scan_2\Scan_2.msot';

slice_idx = 1;
wl_idx = 2;

[Reconstruction_slice] = read_data_slice (recon_path, slice_idx, wl_idx);

sample_name = Reconstruction_slice.datainfo.Name;
recon_name = Reconstruction_slice.datainfo.ReconNode.Name;
wl = int2str (Reconstruction_slice.wl_proc_value);
slice = int2str (slice_idx);

%% basic analysis

[basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Reconstruction_slice.Recon);

%% display basic analysis values

plot_title = strcat('ScanName =', strrep(sample_name,'_',' '), '; wl = ',wl,', slice = ', int2str(slice_idx) );

if several_days % 1 if several days for sample !!!
    plot_title = strcat(plot_title, ', day ', int2str(day_ind));
end

plot_basic_analysis_values (basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img, plot_title);


%% choose limits for residuals

limit_for_normal = 0.35;
limit_for_reversed = 0.35;

imagesc ((residuals.normal<limit_for_normal).*residuals.normal+(residuals.reversed<limit_for_reversed).*residuals.reversed); colorbar;

%% exp fit (for chosen residuals' limits)

Mask_normal = residuals.normal < limit_for_normal;
Mask_reversed = residuals.reversed < limit_for_reversed;
pulses_num = size(trajectories.mean_cycle_normalized,3);
%pulses_num = 11;

[exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);

%% by cycles analysis

delta.begining = 1; % for cycle first index (from end for reversed ones) 
delta.end = 10;      % for cycle last index  (from end for reversed ones)

cycles_analysis_values = get_cycles_analysis_values (Reconstruction_slice.Recon, delta);

%% save processed data


savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
save (savename, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','sample_name','recon_name','wl','slice_idx','-v7.3');

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
save (savename, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','-v7.3');

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
save (savename, 'cycles_analysis_values','sample_name','recon_name','wl','slice_idx','-v7.3');
