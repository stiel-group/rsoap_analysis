*For running all of the scripts load the current functions folder first and locate it in the same working directory*

1. For performing the full basic analysis run calculation.m with the following input parameters
calculation (data_path, sample_name, slice_idx, wl_idx)
where:
data_path - the path of the input .mat file
sample_nam - the name of your scan
slic_idx - index of processed slice
wl_idx - index of processe wavelength (2 by default)

the script runs automatically, although on the step of roi of fluecy deffinition thee user has to click on the anatomy image, to establish the edges of the sample area

as the result there are four matrices containing the basic analysis values are created:
sample_name_slice_idx_wl_idx_basic_analysis_values.mat
sample_name_slice_idx_wl_idx_exp_fit.mat
sample_name_slice_idx_wl_idx_cycles_analysis_values.mat
sample_name_slice_idx_wl_idx_roi_fluency.mat

* for further refference in other scripts user needs to know only sample_name and slice and wavelength index, no further specifications needed *

1* for performing only parts of basic analysis or ploting their results run full_analysis_script.m
1.1* adjust to your scan as following in
recon_path = 'data.mat';
slice_idx = 1;
wl_idx = 2;
1.2* run all the other parts of the script consecutively for full analysis or selected one if needed to rerun previously analysed scans


2. For training a new model run training.m function with the following parameters

training (model_name, sample_name, slice_idx, wl_idx, already_existing);

model_name - name of trained model
sample_name - name of sample used for training
slice_idx - slice index of sample used for training
wl_idx - avellength index used for training (2 by default)
already_existing - 0 if the new model is created; 1 if the new sample is added for model retraining

the script runs automatically, although on the step of roi of fluecy deffinition thee user has to click on the anatomy image, to establish 
 1) the edges of the sample area
 2) the edges of the switching roi
 
As the result 'model_name.mat' file is created, that can be further used for unmixingon step 3

2* for performing only parts of basic analysis or ploting their results run build_ml_model.m 
2.1* Adjust enter valuse as following
 loadfolder = '..\'; 
 sample_name = 'sample_name';
 wl = '770';
 slice = '4';
 for each scan slice used for training
 And re-run part
 %% load data - repeat for every scan used for model
2.2* Then run part
 %% build decision tree based on factor values

3. For performing ml-based analysis run factors_analysis.m 
3.1 Load the model to use for analysis (either uplloaded from 'ML_models' folder or created on step 2)
load ('Mdl_name.mat','Mdl');
3.2 Adjust analysed scan info
loadfolder = '..\'; 
sample_name = 'sample_name';
wl = '770';
slice = '1';
3.3 Run the rest of the script

as the result sample_name_mask.mat file will be created, that contains the mask of swithing area

4. To save the results as .png images run save_imgs_png.m

As th result following images will be created and saved in the working directory

sample_name_Anatomy.png - the anatomy image
sample_name_clusters.png - the unmixed mask for switching area
sample_name_rsqr.png - the quality of exponential fitting for the unmixed switching area
sample_name_kinetics.png - the kinetics for the switching are
and
sample_name_clusters_overlayed.png - the unmixed mask for switching area overlayed over anatomy image
sample_name_rsqr_overlayed.png - the quality of exponential fitting for the unmixed switching area overlayed over anatomy image
sample_name_kinetics_overlayed.png - the kinetics for the switching are overlayed over anatomy image


