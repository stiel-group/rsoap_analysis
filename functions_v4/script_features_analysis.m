loadfolder = 'H:\2_basic_analysis_values_mat\features_values_training\';

params_values = [];
data_labels = [];

sample_names = ["1766_4t1_RePCM_Ear30_day_1","1766_4t1_RePCM_Ear30_day_9","RePCM_Bacteria_Conc1","RePCM_Bacteria_Conc2"];
slices = ['7', '6' ,'3' ,'2' ];

factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16 17:27]; % exp norm and exp reversed as 2 values

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 16 17:27]; % exp both as 1 value

%factors_to_include = [1 4 5 7 8 9 10 11 12 13 14 15 16 17:27]; % exp both as 1 value and is normal or reversed

%factors_to_include = [17:27]; % only trajectory

%factors_to_include = [1 2 3 4 5 7 8 9 10 11 12 13 16]; % as previously + depth

%factors_to_include = [1 2 3 4 5 13 16 17:27]; % without cycle analyses values

%factors_to_include = [1 2 3 4 5 13 16 17:18] ; % only basic

for sample_ind = 1:size(sample_names,2)

    loadname = strcat (loadfolder,sample_names(1,sample_ind),'_wl_',wl,'_slice_',slices(sample_ind),'_features_values.mat');
    load(loadname,  'Factors_values');

    loadname = strcat (loadfolder,sample_names(1,sample_ind),'_wl_',wl,'_slice_',slices(sample_ind),'_classes_labels.mat');
    load(loadname,  'Class_lables');
     
    % balance labels here for each sample
    % (we assume that always less signa)
    number_signal_points = sum(Class_lables == 's');
    number_bg_points = sum(Class_lables == 'b');
    
    if (number_bg_points>number_signal_points)
        [pos_bg] = find(Class_lables == 'b');
        [pos_s] = find(Class_lables == 's');
        rand_ind = randperm(number_bg_points, number_signal_points);
        Factors_values_balanced = [Factors_values(pos_s,factors_to_include);Factors_values(pos_bg(rand_ind),factors_to_include)];
        Class_lables_balanced = [Class_lables(pos_s);Class_lables(pos_bg(rand_ind))];
        params_values = [params_values; Factors_values_balanced];
        data_labels = [data_labels; Class_lables_balanced];
    else 
        
        % if more signal points or equal - keep all
        
        params_values = [params_values; Factors_values(:,factors_to_include)];
        data_labels = [data_labels; Class_lables(:)];
        
    end

end


%% PCA

params_values_normalized = normalize(params_values,2,'range');

[coeff,score,latent,tsquared,explained,mu] = pca(params_values_normalized);

% if for all params
%Warning: Columns of X are linearly dependent to within machine precision.
%Using only the first 23 components to compute TSQUARED. 
 %%
 plot(explained)
 sum(explained>0.05)
 %% 9 number of useful components - 1st type of factors used
 imagesc(coeff(:,1:4));colorbar;
 %%
 
scatter(score(:,1),score(:,2))
axis equal
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
 
%%

savefolder = 'H:\2_basic_analysis_values_mat\ml_models\pca\';

model_name = 'without_trajectories';

savename = strcat (savefolder,model_name,'_pca.mat');
save(savename, 'params_values_normalized', 'factors_to_include','coeff','score','latent','tsquared','explained','mu','-v7.3');


%% load pca

loadfolder = 'H:\2_basic_analysis_values_mat\ml_models\pca\';

model_name = 'without_cycles_analysis';

loadname = strcat (loadfolder,model_name,'_pca.mat');
load(loadname, 'params_values_normalized', 'factors_to_include','coeff','score','latent','tsquared','explained','mu');

%%

params_values_normalized = normalize(params_values,2,'range');

autoenc = trainAutoencoder(params_values_normalized',2);

%% load test data

loadfolder = 'H:\2_basic_analysis_values_mat\features_values_training\';
sample_name = '1763_HCT116_RePCM_Ear3_day_14'
slice = '11';
loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_features_values.mat');
save(loadname,  'Factors_values');

test_factors_values= [];
test_factors_values = Factors_values(:,factors_to_include);
test_factors_normalized = normalize(test_factors_values,2,'range');


dataReconstructed = predict(autoenc,test_factors_normalized');

%%

error = mse(dataReconstructed - test_factors_normalized')

 %%     0.0054 11 weights
 %%   0.0059 - 4 feature
 %%   0.0063 -- 1
 
 
