For performing ml with pca
1. call script_label_groundtruth for adding new scan slice to dataset
2. call script_features_analysis to get pca and encoder
   speify data used as following
   sample_names = ["1766_4t1_RePCM_Ear30_day_1","1766_4t1_RePCM_Ear30_day_9","RePCM_Bacteria_Conc1","RePCM_Bacteria_Conc2", "Conc_4_Conc_5","Conc_6_Conc_7", "Re_Bead", "ReJurkat_Con1","ReJurkat_Con2","ReJurkat_Con3","ReJurkat_Con4","ReJurkat_Con5","ReJurkat_Con6","brain_new_mouse_1","1763_HCT116_RePCM_Ear3_day_2","1763_HCT116_RePCM_Ear3_day_3","1763_HCT116_RePCM_Ear3_day_7","1763_HCT116_RePCM_Ear3_day_9","1763_HCT116_RePCM_Ear3_day_11","1763_HCT116_RePCM_Ear3_day_14","Conc1_2_3__1796_Switching","Conc1_2_3__1796_Switching","Jurkats_old_Conc_1","Jurkats_old_Conc_2","Conc2_1791_Jurkats_RePCM_Switching","Conc1_Jurkats_RePCM_Switching"];
   slices = ["7", "6" ,"3" ,"2","2","3","5","2","3","1","3","3","2","5","8","10","8","11","7","11","5","14","6","4","7","7"];
   factors_to_include = [1 2 3 4 5 13 17:27];
   order of factors:
   1.Rsqr
   2.Exp b normal
   3.Exp b reversed
   4.Mean intensities
   5.Amplitudes
   6.Negatives (!not included if normalised and shifted)
   7.Ffts
   8.Max pos median
   9.Min pos median
   10.Max pos std
   11.Min pos std
   12.Good cycles num
   13.Cycles length (till fades to noise)
   14.Exp both together
   15.is normal or reversed
   16.Distance to surface
   17-27Trajectorz points 1/11
   is_pca = 1;
   is_encoder = 1;
   model_name = 'without_cycles_analysis_pca';
   
3.1 call script_build_dt_model
3.2 call script_build_dt_model_on_pca
    specify model_name saved from previous step

Built models can be used samewise in main scripts