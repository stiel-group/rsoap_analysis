added: 
1. pca analysis
2. encoder built
3. groundtruth automatizaion saved as .mat
4. features set saved as .mat file

changed:
1. building ml model, now 2 functions:
   build_ml_model
   build_ml_model_on_pca

2. features processing divided into separate testing, validation, actual processing
