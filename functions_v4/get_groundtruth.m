function [p_roi, background] = get_groundtruth (Anatomy_img)

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
p_roi = roipoly;
close(figure(1));

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
background = roipoly;
close(figure(1));

end