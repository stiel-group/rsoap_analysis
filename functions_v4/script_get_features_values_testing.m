loadfolder = 'H:\2_basic_analysis_values_mat\basic_analysis_unified\'; 

sample_name = '1763_HCT116_RePCM_Ear3_day_11';
wl = '770';
slice = '7';

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_exp_fit.mat');
load (loadname, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','recon_name','wl');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
load (loadname, 'basic_analysis_values','ffts');

loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_cycles_analysis_values.mat');
load (loadname, 'cycles_analysis_values');


%% get depth

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
roi_mouse = roipoly;
close(figure(1));

B = bwboundaries(roi_mouse);
roi_border = B{1};

roi_min_distances = zeros(size(roi_mouse)); % 0 if not in roi

[roi_point_x, roi_point_y] = ind2sub(size(roi_mouse),find(roi_mouse == 1));

smallest_distances = (pdist2(roi_border,[roi_point_x, roi_point_y],'euclidean','Smallest',1));


for point_ind=1:size(roi_point_x,1)
    roi_min_distances (roi_point_x(point_ind),roi_point_y(point_ind)) = smallest_distances(point_ind);
end


%% get all features values
number_points = sum(roi_mouse(:)); %only points inside mouse 
x_coord = zeros(number_points, 1);
y_coord = zeros(number_points, 1);

% factors:
% rsqr 
% exp.b normal
% exp.b reversed
% mean_intensities
% amplitudes
% neative_vals_count
% max_pos_median
% min_pos_median
% max_pos_std
% min_pos_std
% good_cycles_num
% cycle_length
% exp.b both
% is normal or reversed
% 11 points from mean normalized cycle

number_factors = 31;
Factors_values = zeros(number_points, number_factors);
Class_lables = char(number_points,1);



[pos_x, pos_y] = find(roi_mouse);


pulses_num = 11;

for p_ind = 1:size(pos_x,1)
    x_ind = pos_x(p_ind);
    y_ind = pos_y(p_ind);
    Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
    Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
    Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
    Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
    Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
    Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
    Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
    Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
    Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
    Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
    Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
    Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
    Factors_values (p_ind, 13) = ffts(x_ind,y_ind);
    Factors_values (p_ind, 14) = exp_fit_both.b(x_ind,y_ind);
    Factors_values (p_ind, 15) = exp_fit_normal.rsqr(x_ind,y_ind) > exp_fit_reversed.rsqr(x_ind,y_ind); 
    Factors_values (p_ind, 16) = roi_min_distances(x_ind,y_ind); 
    % add mean trajectories
    for time_point = 1:pulses_num
           Factors_values (p_ind, 16+time_point) = trajectories.mean_cycle(x_ind,y_ind,time_point); 
    end
    
    coord.x(p_ind) = x_ind;
    coord.y(p_ind) = y_ind;

end


savefolder = 'H:\2_basic_analysis_values_mat\features_values_testing\';

savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice,'_features_values.mat');
save(savename,  'Factors_values','coord','-v7.3');
