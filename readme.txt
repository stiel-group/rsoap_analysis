The code performs machine learning–based analysis of optoacoustic images for detection of reversibly switchable optoacoustic proteins.

The input .mat file should conatin an already reconstructed time series of optoacoustic images in the following format:
an array with dimensions: cycles*slices*wavelength*pulses

An output of the processing is a .png image with a colorcoded segmentation result with clases as switchers used and background areas area.

A user has a possibility to train their own decision model as well as use alread existing one.

For the usage of basic functionality:

1. Download 'Scripts' folder and make it current folder in matlab
2. Download 'functions' folder an locate it iside the current folder
3. See further details on different functionality usage in 'Scripts' folder 'readme.txt'

For the usage of already pre-trained models:

- Download the correspondion model from 'ML-model' and set it'S location, corresponding to the instructions give for the used script

For the further functionality 

- Upload the needed version of additional functions from corresponding 'functions_vN' folder and locate them inside the main 'functions' folder

Versions of functions are following:

version 1: basic analysis, building binary classification model
version 2: with data usage analysis added (defining suitable number of cycles and pulses, providing standard measure of the quality of classifications) 
version 3: with multilabeling added, multiple slice processing together added, adding training data to existing model
version 4: with pca and parameters used analysis, groundtruth automatizaion, features processing divided eparately on 
