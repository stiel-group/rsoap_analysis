%% get_info_conent_script

%% load data

loadfolder = 'C:\full_basic_analysis\';
%loadfolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\'; 

%sample_name = 'Jurkats_old_Conc_1';
%sample_name = 'De_Bacteria';
sample_name = 'm199_Ear3_Day_14_switching';
%sample_name = strcat(sample_name,'_rigid_movement_corrected');

wl = '770';
slice = '19';

several_days = 0; % 1 if several days for sample !!!
day_ind = 14;      % day ind, for several days for sample !!!
%%

if ~several_days % 1 if several days for sample !!!
    loadname = strcat (loadfolder,sample_name,'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    load (loadname, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','recon_name','wl','slice_idx','several_days');
else
    loadname = strcat (loadfolder,sample_name,'_day_',int2str(day_ind),'_wl_',wl,'_slice_',slice,'_basic_analysis_values.mat');
    load (loadname, 'Anatomy_img', 'trajectories', 'intensities_concatenated','basic_analysis_values','residuals','ffts','recon_name','wl','slice_idx','several_days','day_ind');
end

%% 
Recon = permute(reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3)),[1 2 4 3]);

%%
[basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Recon);

%%
plot_title = strcat('ScanName =', strrep(sample_name,'_',' '), '; wl = ',wl,', slice = ', int2str(slice_idx) );

if several_days % 1 if several days for sample !!!
    plot_title = strcat(plot_title, ', day ', int2str(day_ind));
end

plot_basic_analysis_values (basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img, plot_title);

%%



limit_for_normal = 0.3;
limit_for_reversed = 0.3;

imagesc ((residuals.normal<limit_for_normal).*residuals.normal+(residuals.reversed<limit_for_reversed).*residuals.reversed); colorbar;
%% get roi

[p_roi, mouse_region] = get_roi (Anatomy_img);

%%
Mask_normal = (residuals.normal < limit_for_normal).*mouse_region;
Mask_reversed = (residuals.reversed < limit_for_reversed).*mouse_region;
pulses_num = size(trajectories.mean_cycle_normalized,3);
pulses_num = 11;
%%
[exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);

%%

%% display exp fit

plot_title = strcat('ScanName =', strrep(sample_name,'_',' '), '; wl = ',wl,', slice = ', int2str(slice_idx) );

if several_days
    plot_title = strcat(plot_title, ', day ', int2str(day_ind));
end

plot_exp_fit (exp_fit_normal, exp_fit_reversed, exp_fit_both,trajectories.mean_cycle_normalized,plot_title);
%%
imagesc(exp_fit_normal.rsqr>0);
rsqr_limit = 0.9;
%% cycles analysis

delta.begining = 1; % for cycle first index (from end for reversed ones) 
delta.end = 10;      % for cycle last index  (from end for reversed ones)

intentsity_cycles = reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3));

cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta);

%%

good_cycles_num_lim = 0;
include_reversed = 0;
mask = get_selection_mask (rsqr_limit,good_cycles_num_lim,include_reversed,exp_fit_normal,exp_fit_both,cycles_analysis_values);
imagesc(mask.indexes);

%%

savefolder = 'C:\full_basic_analysis\for_info_content\';
savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice);

all_cycles = size(Recon,3);
all_pulses = size(Recon,4);

for cycles_num = all_cycles
    for pulses_num = 26
        [basic_analysis_values, trajectories, intensities_concatenated, residuals, ffts, Anatomy_img] = get_basic_analysis_values (Recon(:,:,1:cycles_num,1:pulses_num));
        
        limit_for_normal   = 0.3;
        limit_for_reversed = 0.3;
        
        Mask_normal = (residuals.normal < limit_for_normal).*mouse_region;
        Mask_reversed = (residuals.reversed < limit_for_reversed).*mouse_region;
        
        [exp_fit_normal, exp_fit_reversed, exp_fit_both ] = get_exp_fit (trajectories.mean_cycle_normalized, Mask_normal, Mask_reversed, pulses_num);
        
        delta.begining = 1; % for cycle first index (from end for reversed ones) 
        delta.end = 10;      % for cycle last index  (from end for reversed ones)
        intentsity_cycles = reshape(intensities_concatenated, size(intensities_concatenated,1), size(intensities_concatenated,1), size(trajectories.mean_cycle,3),  size(intensities_concatenated,3)/size(trajectories.mean_cycle,3));

        cycles_analysis_values = get_cycles_analysis_values (intentsity_cycles, delta);

        current_savename = strcat (savename,'_cycles_',int2str(cycles_num),'_pulses_',int2str(pulses_num),'_exp_fit.mat');
        save (current_savename, 'Anatomy_img','limit_for_normal','limit_for_reversed', 'exp_fit_normal', 'exp_fit_reversed', 'exp_fit_both','trajectories','sample_name','recon_name','wl','slice_idx','several_days','day_ind','-v7.3');
        figure(pulses_num);
        imagesc(exp_fit_both.b,[-0.3, 0]);
        %rsqr_limit = 0.6;        % R-square for exp fitting quality
        %include_reversed = 1;    % 1 if included 
        %good_cycles_num_lim = 0; % 0 if not limited

        
        
        rsqr = [];
        exp_b_normal = [];
        exp_b_reversed = [];
        mean_intensities = [];
        amplitudes = [];
        negative_vals_count = [];
        max_pos_median = [];
        min_pos_median = [];
        max_pos_std = [];
        min_pos_std = [];
        good_cycles_num = [];
        cycle_length = [];
        ffts_right_freq = [];
        
        p_roi_estimated = p_roi.* (exp_fit_both.rsqr~=0);
        mouse_region_estimated = mouse_region.* (exp_fit_both.rsqr~=0);


        number_points = sum(mouse_region_estimated(:)); %only points for which exp fit was estimated 
        x_coord = zeros(number_points, 1);
        y_coord = zeros(number_points, 1);
        
        number_factors = 13;
        Factors_values = zeros(number_points, number_factors);

        x_size = size(Anatomy_img, 1);
        y_size = size(Anatomy_img, 2);
    
        p_ind = 0;

        for x_ind = 1:x_size
            for y_ind = 1:y_size
                if (mouse_region_estimated(x_ind,y_ind))
                    p_ind = p_ind + 1;
                    x_coord(p_ind) =  x_ind;
                    y_coord(p_ind) =  y_ind;
                    Factors_values (p_ind, 1) = exp_fit_both.rsqr(x_ind,y_ind);
                    Factors_values (p_ind, 2) = exp_fit_normal.b(x_ind,y_ind);
                    Factors_values (p_ind, 3) = exp_fit_reversed.b(x_ind,y_ind);
                    Factors_values (p_ind, 4) = basic_analysis_values.mean_intensities(x_ind,y_ind);
                    Factors_values (p_ind, 5) = basic_analysis_values.amplitudes(x_ind,y_ind);
                    Factors_values (p_ind, 6) = basic_analysis_values.negative_vals_count(x_ind,y_ind);
                    Factors_values (p_ind, 7) = cycles_analysis_values.max_pos_median(x_ind,y_ind);
                    Factors_values (p_ind, 8) = cycles_analysis_values.min_pos_median(x_ind,y_ind);
                    Factors_values (p_ind, 9) = cycles_analysis_values.max_pos_std(x_ind,y_ind);
                    Factors_values (p_ind, 10) = cycles_analysis_values.min_pos_std(x_ind,y_ind);
                    Factors_values (p_ind, 11) = cycles_analysis_values.good_cycles_num(x_ind,y_ind);
                    Factors_values (p_ind, 12) = cycles_analysis_values.cycle_length(x_ind,y_ind);
                    Factors_values (p_ind, 13) = ffts(x_ind,y_ind);
                end
            end 
        end

        rsqr = [rsqr; Factors_values(:, 1) ];
        exp_b_normal = [exp_b_normal; Factors_values(:, 2)];
        exp_b_reversed = [exp_b_reversed; Factors_values(:, 3)];
        mean_intensities = [mean_intensities; Factors_values(:, 4)];
        amplitudes = [amplitudes; Factors_values(:, 5)];
        negative_vals_count = [negative_vals_count; Factors_values(:, 6)];
        max_pos_median = [max_pos_median; Factors_values(:, 7)];
        min_pos_median = [min_pos_median; Factors_values(:, 8)];
        max_pos_std = [max_pos_std; Factors_values(:, 9)];
        min_pos_std = [min_pos_std; Factors_values(:, 10)];
        good_cycles_num = [good_cycles_num; Factors_values(:, 11)];
        cycle_length = [cycle_length; Factors_values(:, 12)];
        ffts_right_freq = [ffts_right_freq; Factors_values(:, 13)];

        FactorTable_test = table(rsqr,exp_b_normal,exp_b_reversed,ffts_right_freq,mean_intensities,amplitudes,negative_vals_count,max_pos_median,min_pos_median,max_pos_std,min_pos_std,good_cycles_num,cycle_length);
        prediction_cart_bag = Mdl.predict (FactorTable_test);
        %prediction_cart_bag = Mdl.predictFcn (FactorTable_test);

        img = zeros(x_size, y_size, 3);
        mask = zeros(x_size, y_size);

        for i=1:size(prediction_cart_bag)
            if (prediction_cart_bag{i} == 's')
                img(x_coord(i),y_coord(i)) = 1;
                mask(x_coord(i),y_coord(i)) = 1;
            end
        end

        figure(pulses_num * 10);
        imshow(img);
        disp(strcat('cycles ',int2str(cycles_num),', pulses ',int2str(pulses_num)));
    %    mask = get_selection_mask (rsqr_limit,good_cycles_num_lim,include_reversed,exp_fit_normal,exp_fit_both,cycles_analysis_values);

        current_savename = strcat (savename,'_cycles_',int2str(cycles_num),'_pulses_',int2str(pulses_num),'_selection_mask.mat');
        save (current_savename,'mask','-v7.3');

    end
end

%%

savefolder = 'H:\2_basic_analysis_values_mat\full_basic_analysis\';
savename = strcat (savefolder,sample_name,'_wl_',wl,'_slice_',slice);


%% calculate tp,fp,tn,fn for all cycles/pulses num

%all_cycles = 47;
%all_pulses = 30;
%all_cycles = 25;

slice_ind = 3;

tp = zeros (all_pulses,all_cycles);
fp = zeros (all_pulses,all_cycles);
tn = zeros (all_pulses,all_cycles);
fn = zeros (all_pulses,all_cycles);

background_region = mouse_region - p_roi;


for cycles_num = 1:all_cycles
     for pulses_num = 2:all_pulses
        
        current_loadname = strcat (savename,'_cycles_',int2str(cycles_num),'_pulses_',int2str(pulses_num),'_selection_mask.mat');
        load (current_loadname,'mask');
        
        imagesc(mask);

  %      tp (pulses_num, cycles_num) = sum(sum(mask.indexes .* p_roi) );
  %      fp (pulses_num, cycles_num) = sum(sum(mask.indexes .* background_region) );
  %      fn (pulses_num, cycles_num) = sum(sum((~mask.indexes) .* p_roi ));
  %      tn (pulses_num, cycles_num) = sum(sum((~mask.indexes) .* background_region));
         tp (pulses_num, cycles_num) = sum(sum(mask .* p_roi) );
         fp (pulses_num, cycles_num) = sum(sum(mask .* background_region) );
         fn (pulses_num, cycles_num) = sum(sum((~mask) .* p_roi ));
         tn (pulses_num, cycles_num) = sum(sum((~mask) .* background_region));
        
    end
end

save_name  = strcat ('C:\full_basic_analysis\info_content\', '_', sample_name,'_slice',int2str(slice_ind), '_statistic_values.mat');

save (save_name, 'tp', 'fn', 'fp', 'tn', '-v7.3');
%%

load_name  = strcat ('C:\full_basic_analysis\info_content\', '_', sample_name,'_slice',int2str(slice_ind),'_statistic_values.mat');

load (load_name, 'tp', 'fn', 'fp', 'tn');
%%  calculat measures

TPR = tp./(tp+fn);
FPR = fp./(fp+tn);
ACC = (tp + tn)./(tp + tn + fp + fn);
MCC = (tp.*tn - fp.*fn) ./ (((tp+fp).*(tp+fn).*(tn+fp).*(tn+fn)).^0.5);
%% plot measures

ay = 2:all_pulses;
ax = 1:all_cycles;


figure (1);
subplot(2,2,1);
imagesc  (ax, ay,TPR(ay,ax)); colorbar;
title(strcat('true positive rate'));
%axis off;
F = getframe(gca);
imwrite(F.cdata, 'true_positive_rate.tif');

subplot(2,2,2);
imagesc  (ax, ay,FPR(ay,ax)); colorbar;
title(strcat('false positive rate'));
%axis off;
F = getframe(gca);
imwrite(F.cdata, 'false_positive_rate.tif');

subplot(2,2,3);
imagesc  (ax, ay,ACC(ay,ax)); colorbar;
title(strcat('accuracy'));
%axis off;
F = getframe(gca);
imwrite(F.cdata, 'accuracy.tif');


subplot(2,2,4);
imagesc  (ax, ay,MCC(ay,ax)); colorbar;
title(strcat('MCC'));
%axis off;
F = getframe(gca);
imwrite(F.cdata, 'matthews_correlation_coef.tif');

%%
figure (2);
subplot(2,2,1);
imagesc  (ax, ay,tp(ay,ax)); colorbar;
title(strcat('true positives'));

subplot(2,2,2);
imagesc  (ax, ay,tn(ay,ax)); colorbar;
title(strcat('true negatives'));

subplot(2,2,3);
imagesc  (ax, ay,fp(ay,ax)); colorbar;
title(strcat('false positives'));

subplot(2,2,4);
imagesc  (ax, ay,fn(ay,ax)); colorbar;
title(strcat('false negatives'));
