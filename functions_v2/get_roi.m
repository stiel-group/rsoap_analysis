function [p_roi, mouse_region] = get_roi (Anatomy_img)

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
mouse_region = roipoly;
close(figure(1));

initial_image = Anatomy_img;
figure(1); imagesc(initial_image); colormap('gray');
p_roi = roipoly;
close(figure(1));

end
