1. Edit analysed scan slice  description

loadfolder = 'C:\full_basic_analysis\';
sample_name = 'm199_Ear3_Day_14_switching';
wl = '770';
slice = '19';
several_days = 0; % 1 if several days for sample !!!
day_ind = 14;      % day ind, for several days for sample !!!

2. Edit save folder destination

savefolder = 'C:\full_basic_analysis\for_info_content\';

3. Run all the rest of script

3*. Adjust the preffered for your analysis cycles and pulses if not all needed

for cycles_num = 1:all_cycles
    for pulses_num = 1:all_pulses
